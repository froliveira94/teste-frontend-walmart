angular.module("testeFrontend").factory("userAPI", function ($http) {
    var _getUsers = function () {
        return $http.get("http://localhost:3000/api/user.json");
    };

    return {
        getUsers: _getUsers
    }
});