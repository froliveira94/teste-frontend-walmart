angular.module("testeFrontend").factory('getPageService', ['$location', function($location) {
    var _getPage = function() {
        var url = $location.url();
        var page = url.replace('/', '');  
        return page;
    }

    return {
        getPage: _getPage
    }
}]);