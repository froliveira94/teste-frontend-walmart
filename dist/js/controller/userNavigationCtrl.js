angular.module("testeFrontend").controller("userNavigationCtrl", function ($scope, $http, userAPI, getPageService) {
    $scope.usuario = [];
    $scope.states = {};
    $scope.states.activeItem = 'item1';
    $scope.items = [
        {
            id: 1,
            title: 'Sua Conta',
            name: 'account'
        },
        {
            id: 2,
            title: 'Pedidos',
            name: 'purchase'
        },
        {
            id: 3,
            title: 'Endereços cadastrados',
            name: 'addresses'
        }
    ];

    userAPI.getUsers().then(function (response) {
        var data = response.data;
        var status = response.data;

        $scope.usuario = data;
    });
});