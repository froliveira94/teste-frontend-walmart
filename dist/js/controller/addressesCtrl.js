angular.module("testeFrontend").controller("addressesCtrl", function ($scope, userAPI) {
    $scope.addresses = [];

    userAPI.getUsers().then(function (response) {
        var data = response.data;
        var status = response.status;
        $scope.addresses = data.address;
    });
});