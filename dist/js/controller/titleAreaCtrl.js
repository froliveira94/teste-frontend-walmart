angular.module("testeFrontend").controller('titleAreaCtrl', ['$scope','getPageService', function($scope,getPageService) {
    var page = getPageService.getPage();
    $scope.infoTitle = {icon: '', title: '', class: '', description: ''};
    switch(page) {
        case 'purchase':
            $scope.infoTitle = {icon: 'fa fa-purchase', title: 'Pedidos', class: 'theme-purchase', description: ''};
            break;
        case 'account':
            $scope.infoTitle = {icon: 'fa fa-account', title: 'Bem vindo a sua conta ;)', class: 'theme-account', description: 'Mantenha sempre seus dados atualizados, fica mais fácil para nós conversarmos'};
            break;
        case 'addresses':
            $scope.infoTitle = {icon: 'fa fa-addresses', title: 'Endereços cadastrados', class: 'theme-addresses', description: 'Mantenha sempre seus dados atualizados, fica mais fácil para nós fazermos as entregas'};   
            break;
        default:  
            $scope.infoTitle = {icon: 'fa fa-account', title: 'Bem vindo a sua conta ;)', class: 'theme-account', description: 'Mantenha sempre seus dados atualizados, fica mais fácil para nós conversarmos'};
    }
}]);