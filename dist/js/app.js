
angular.module("testeFrontend", ['ngRoute']).config(function($routeProvider){
    $routeProvider.when('/', {
        templateUrl: 'view/account.html'
    }).when('/account', {
        templateUrl: 'view/account.html'
    }).when('/purchase', {
        templateUrl: 'view/purchase.html'
    }).when('/addresses', {
        templateUrl: 'view/addresses.html'
    }).otherwise({ redirectTo: '/' });
}); 